using System;

namespace ProbeNet.Export.Loader
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = false)]
    public class BaseClassAttribute: Attribute
    {
        public BaseClassAttribute (Type baseClassType)
        {
            BaseClassType = baseClassType;
        }

        public Type BaseClassType {
            get;
            private set;
        }
    }
}

