using Frank.Database.Connection;
using Internationalization;
using Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace ProbeNet.Export.Loader
{
    public static class ExportLoader
    {
        const string ExportDllNamePattern = "probenet-export-*.dll";
        const string DatabaseConnectionDllNamePattern = "database-connection-*.dll";

        private static IDictionary<ExportType, IList<IExportInformation>> exportsCache;
        private static IList<DatabaseConnection> databaseConnections;

        public static void LoadAllExports(I18n i18n)
        {
            foreach (object o in Enum.GetValues(typeof(ExportType))) {
                ExportType exportType = (ExportType)o;
                IList<IExportInformation> exports = Load(exportType, i18n);
                Logger.Log("Loaded {0} {1} export backends", exports.Count, exportType);
            }
        }

        public static IList<IExportInformation> Load(ExportType exportType)
        {
            if (exportsCache == null || !exportsCache.ContainsKey(exportType)) {
                throw new ArgumentException("No exports loaded yet, you need to call Load(exportType, i18n) first.");
            }
            return exportsCache[exportType];
        }

        public static IList<IExportInformation> Load(ExportType exportType, I18n i18n)
        {
            if (exportsCache == null) {
                exportsCache = new Dictionary<ExportType, IList<IExportInformation>>();
            }
            if (exportsCache.ContainsKey(exportType)) {
                return exportsCache[exportType];
            }

            IList<IExportInformation> exports = new List<IExportInformation>();

            string[] files = Directory.GetFiles(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), ExportDllNamePattern);
            foreach (string fileName in files) {
                try {
                    Assembly assembly = Assembly.LoadFile(fileName);

                    MemberInfo member = typeof(ExportType).GetField(exportType.ToString());
                    Attribute[] attributes = Attribute.GetCustomAttributes(member);
                    if (attributes.Length > 0 && attributes[0] is BaseClassAttribute) {
                        string[] resources = assembly.GetManifestResourceNames();
                        IEnumerable<string> i18nResources = resources.Where(s => s.EndsWith("i18n.en.json"));
                        if (i18nResources.Count() > 0) {
                            i18n.InitializeAssembly(assembly);
                        }
                        Type baseClass = (attributes[0] as BaseClassAttribute).BaseClassType;
                        Type[] allTypes = assembly.GetExportedTypes();
                        foreach (Type type in allTypes) {
                            if (type.IsSubclassOf(baseClass)) {
                                ExportInformation information = new ExportInformation(type);
                                exports.Add(information);
                            }
                        }
                    }
                } catch (Exception e) {
                    Logger.Error("Exception while loading export from file {0}: {1}", fileName, e.Message);
                }
            }
            exportsCache.Add(exportType, exports);

            return exports;
        }

        public static void LoadDatabaseConnections(I18n i18n)
        {
            if (databaseConnections == null) {
                databaseConnections = new List<DatabaseConnection>();
                string[] files = Directory.GetFiles(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location),
                    DatabaseConnectionDllNamePattern);
                foreach (string fileName in files) {
                    try {
                        Assembly assembly = Assembly.LoadFile(fileName);

                        Type baseClass = typeof(DatabaseConnection);
                        Type[] allTypes = assembly.GetExportedTypes();
                        i18n.InitializeAssembly(assembly);
                        foreach (Type type in allTypes) {
                            if (type.IsSubclassOf(baseClass)) {
                                databaseConnections.Add(Activator.CreateInstance(type) as DatabaseConnection);
                            }
                        }
                    } catch (Exception e) {
                        Logger.Error("Exception while loading database connection from file {0}: {1}", fileName, e.Message);
                    }
                }
            }
        }

        public static IList<DatabaseConnection> LoadDatabaseConnections()
        {
            if (databaseConnections == null) {
                throw new ArgumentException("No database connections loaded yet, you need to call LoadDatabaseConnections(i18n) first.");
            }
            return databaseConnections;
        }
    }
}

