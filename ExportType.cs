using ProbeNet.Export.Database;
using ProbeNet.Export.File;
using ProbeNet.Export.Print;

namespace ProbeNet.Export.Loader
{
    public enum ExportType
    {
        [BaseClassAttribute(typeof(PrintExport))]
        Print,

        [BaseClassAttribute(typeof(FileExport))]
        File,

        [BaseClassAttribute(typeof(DatabaseExport))]
        Database
    }
}

