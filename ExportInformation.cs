using Internationalization;
using System;
using System.ComponentModel;
using System.Reflection;

namespace ProbeNet.Export.Loader
{
    public class ExportInformation : IExportInformation
    {
        public ExportInformation(Type type)
        {
            if (!type.IsSubclassOf(typeof(DataExport))) {
                throw new ArgumentException(String.Format("Invalid DataExport type: {0}", type.Name), "type");
            }
            Type = type;
            CaptionAttribute = LoadCaption(type);
            Name = LoadName(type);
            ExportType = LoadExportType(type);
        }

        private static TranslatableCaptionAttribute LoadCaption(Type type)
        {
            object[] attributes = type.GetCustomAttributes(typeof(TranslatableCaptionAttribute), false);
            if (attributes.Length != 1) {
                throw new ArgumentException(String.Format(
                    "Type {0} should have exactly 1 TranslationCaptionAttribute, but has {1}",
                    type.Name, attributes.Length));
            }
            return (TranslatableCaptionAttribute)attributes[0];
        }

        private static string LoadName(Type type)
        {
            object[] attributes = type.GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (attributes.Length != 1) {
                throw new ArgumentException(String.Format(
                    "Type {0} should have exactly 1 DescriptionAttribute, but has {1}",
                    type.Name, attributes.Length));
            }
            return ((DescriptionAttribute)attributes[0]).Description;
        }

        private static ExportType LoadExportType(Type type)
        {
            Type enumType = typeof(ExportType);
            foreach (ExportType entry in (Enum.GetValues(enumType) as ExportType[])) {
                MemberInfo[] memberInfo = enumType.GetMember(entry.ToString());
                if (memberInfo.Length == 0) {
                    throw new Exception(String.Format(
                        "No member with the required value ({0}) found on type {1}", entry.ToString(), type));
                }
                BaseClassAttribute[] attributes =
                    memberInfo[0].GetCustomAttributes(typeof(BaseClassAttribute), false) as BaseClassAttribute[];
                if (attributes.Length != 1) {
                    throw new Exception(String.Format(
                        "The enum value {0} of the ExportType enum has now BaseClass attribute",
                        entry.ToString()));
                }
                if (type.IsSubclassOf(attributes[0].BaseClassType)) {
                    return entry;
                }
            }
            throw new Exception(String.Format(
                "The export {0} is not inherited of any of the base types defined in ExportType",
                type.Name));
        }

        public Type Type
        {
            get;
            private set;
        }

        public TranslatableCaptionAttribute CaptionAttribute
        {
            get;
            private set;
        }

        public string Name
        {
            get;
            private set;
        }

        public TranslationString GetCaption(I18n i18n)
        {
            return CaptionAttribute.GetTranslation(i18n);
        }

        public DataExport CreateExport(SettingsProvider settingsProvider)
        {
            try {
                return (DataExport)Activator.CreateInstance(Type, settingsProvider);
            } catch (Exception e) {
                Logging.Logger.Log(e, "Error while creating data export object");
                return null;
            }
        }

        public T CreateExport<T>(SettingsProvider settingsProvider) where T : DataExport
        {
            if (!Type.IsSubclassOf(typeof(T)) && typeof(T) != Type) {
                throw new ArgumentException(String.Format(
                    "The generic argument {0} must be the same as {1} or one of its parents", typeof(T).Name, Type.Name));
            }
            return (T)CreateExport(settingsProvider);
        }

        public ExportType ExportType
        {
            get;
            private set;
        }
    }
}

